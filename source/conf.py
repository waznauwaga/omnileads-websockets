import os

LOG_LOCATION = os.environ.get('LOG_LOCATION') or '/opt/services/webapp/src'
REDIS_HOSTNAME = os.environ.get('REDIS_HOSTNAME') or 'redis'
REDIS_PORT = os.environ.get('REDIS_PORT') or 6379
WEBSOCKET_PORT = os.environ.get('WEBSOCKET_PORT') or 8000


# For PubSub channels
SUFIJOS_A_CANALES = {
    'contactados': 'CONTACTED',
    'calificados': 'DISPOSITIONED',
    'no_atendidos': 'NOT_ATTENDED',
    'estadisticas_dia_actual': 'CURRENT_DAY_STATS',
    'grabaciones': 'RECORDINGS',
    'formulario_gestion': 'ENGAGED_DISPOSITIONS',
    'resultados_de_base_contactados': 'CONTACTED',
    'resultados_de_base_contactados_todos': 'ALL_CONTACTED'
}

SERVICIOS_A_CANALES = {
    'reporte_grafico_campana': 'STATUS_CSV_REPORT',
    'reporte_agente': 'AGENT_REPORT',
    'genera_zip_grabaciones': 'STATUS_DOWNLOAD',
    'reporte_resultados_de_base_campana': 'BASE_RESULTS_REPORT'
}
