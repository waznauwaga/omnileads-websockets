import json
from aioredis import create_redis, Channel

from conf import REDIS_HOSTNAME, REDIS_PORT, SUFIJOS_A_CANALES,\
    SERVICIOS_A_CANALES

STREAM_SERVICE_URI_NAME = 'stream'
PUB_SUB_SERVICE_NAME = 'pubsub'


class SubscriptionManager(object):

    def __init__(self, websocket, path):
        __, __, servicio, sufijo, report_id, task_id = path.split('/')
        self.websocket = websocket
        if servicio == STREAM_SERVICE_URI_NAME:
            self.service_type = STREAM_SERVICE_URI_NAME
            self.subscription = StreamSubscription(
                websocket, sufijo, report_id, task_id)
        else:
            self.service_type = PUB_SUB_SERVICE_NAME
            self.subscription = PubSubSubscription(
                websocket, servicio, sufijo, report_id, task_id)

    async def subscribe_to_redis(self):
        await self.subscription.subscribe_to_redis()

    async def subscription_message(self):
        await self.subscription.subscription_message()

    async def get_data(self):
        await self.subscription.get_data()

    async def close_connnection(self):
        await self.subscription.close_connection()


class BaseSubscription(object):
    SUBSCRIBE_CONFIRMATION_MESSAGE = 'Subscribed!'

    def __init__(self, websocket, servicio, sufio, reporte_id, task_id):
        self.conn = None
        self.websocket = websocket
        self.servicio = servicio
        self.sufijo = sufio
        self.reporte_id = reporte_id
        self.task_id = task_id

    async def subscribe_to_redis(self):
        self.conn = await create_redis(f'redis://{REDIS_HOSTNAME}:{REDIS_PORT}')

    async def subscription_message(self):
        await self.websocket.send(self.SUBSCRIBE_CONFIRMATION_MESSAGE)

    async def get_data(self):
        raise NotImplementedError()

    async def close_connection(self):
        await self.conn.wait_closed()


class StreamSubscription(BaseSubscription):
    SUBSCRIBE_CONFIRMATION_MESSAGE = 'Stream subscribed!'

    def __init__(self, websocket, sufijo, reporte_id, task_id):
        super(StreamSubscription, self).__init__(
            websocket, STREAM_SERVICE_URI_NAME, sufijo, reporte_id, task_id)

        self.stream_name = f'{self.sufijo}_{self.reporte_id}_{self.task_id}'
        # Se usa para sabes si leo lo último del stream o espero nueva data
        self.read_cache = True

    async def get_data(self):
        if self.read_cache:
            # Es la primera vez, leo todo lo que se ha publicado
            messages = await self.conn.xrevrange(self.stream_name)
            self.read_cache = False
            messages = [(self.stream_name.encode("utf-8"), *m)
                        for m in messages]
        else:
            messages = await self.conn.xread([self.stream_name], timeout=0)

        prepared_messages = []
        for msg in messages:
            payload = list(msg[2].items())[0][1].decode("utf-8")
            # agregar el stream_id para controlar orden de los datos
            stream_id_part = f'\'stream_id\':\'{msg[1].decode("utf-8")}\''
            payload = f'{payload[:-1]},{stream_id_part}' + '}'
            prepared_messages.append(payload)
        await self.websocket.send(json.dumps(prepared_messages))


class PubSubSubscription(BaseSubscription):

    def __init__(self, websocket, servicio, sufijo, reporte_id, task_id):
        super(PubSubSubscription, self).__init__(
            websocket, servicio, sufijo, reporte_id, task_id)
        self.channel = None

    async def subscribe_to_redis(self):
        await super(PubSubSubscription, self).subscribe_to_redis()
        self.conn = self.conn.connection
        self.channel = Channel(self.__path_to_key(), is_pattern=False)
        await self.conn.execute_pubsub('subscribe', self.channel)

    async def get_data(self):
        message = await self.channel.get()
        message_utf8 = message.decode('utf-8')
        await self.websocket.send(message_utf8)

    def __path_to_key(self):
        canal = SERVICIOS_A_CANALES[self.servicio]
        clave = SUFIJOS_A_CANALES[self.sufijo]
        return 'OML:{0}:{1}:{2}:{3}'.format(
            canal, clave, self.reporte_id, self.task_id)

    async def close_connection(self):
        await self.conn.execute_pubsub('unsubscribe', self.channel)
        self.conn.close()
