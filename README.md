# Simple websocket server for Omnileads

A simple websocket server for [OMniLeads](https://gitlab.com/omnileads/ominicontacto), used to offer to OML website realtime communication with some processes of the system using Redis PUBSUB built-in feature.

## Docker image

* **Websockets Version:** 1.3.1
* **Base Image:** python:3.6-alpine

### Build

```
  cd build/docker
  docker build -f Dockerfile -t freetechsolutions/omlwebsockets:$TAG ../..
```
Where $TAG is the docker tag you want for image.

### Run container

```
  docker run -it freetechsolutions/omlwebsockets:latest bash
```

If you need to add environment variables and link folders to container, check docker run documentation: https://docs.docker.com/engine/reference/commandline/run/

**Environment variables needed:**
```
  REDIS_HOSTNAME // hostname of redis service
  REDIS_PORT=6379 // port of redis service
  WEBSOCKET_PORT=8000 // port of websocket service
  LOG_LOCATION // location of log file
```

## RPM

### Build

* **Websockets version:** The Websockets base version is written in file `.websockets_version`

Test the RPM build with these steps:

1. Check variables for container builder in `scripts/.env_buildercontainer` file.
2. Cd into build/rpm
3. Run builder_container.sh script
4. Inside the container, cd again into build/rpm
5. Execute build_rpm.sh script

### Deploy

To deploy Websockets server in a dedicated host two main steps are needed:

1. Install Websockets server in its host, following these steps:

**SO:** Centos7 and derivatives

* Update the machine
```
  yum update -y
```
* Set timezone in accordance where you need it
* Disable selinux if enabled
```
  sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/sysconfig/selinux
  sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
```
* Disable firewalld if enabled
```
  systemctl disable firewalld
  systemctl stop firewalld
```
* Reboot the machine
* Install git
```
  yum install git -y
```
* Clone this repository where you want
```
  git clone https://gitlab.com/omnileads/omnileads-websockets.git
```
* Install ansible in the dedicated host.
```
  yum install python3-pip python3 epel-release -y
  pip3 install pip --upgrade
  pip3 install 'ansible==2.9.2'
```
* Go to `deploy` directory
```
  cd omnileads-websockets/deploy
```
* Open the file ansible/inventory and set there the parameters.
```
  [prodenv-aio:vars]
  ## IP/hostnames/ports of services that interact with websocket server ###
  redis_host=
  redis_port=
  websocket_port=8000
```
* Run ansible-playbook   
```
  ansible-playbook websockets.yml -i inventory --extra-vars "websockets_version=$(cat ../.websockets_version)"
```
2. Install OMniLeads. In your inventory file, edit the parameter websocket_host with the IP of this host and websocket_port if you changed the default port (8000)

---
**WARNING:** If you are going to use s websocket_port different of default, it must be the same port you used in websocket_server
---
